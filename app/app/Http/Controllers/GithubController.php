<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException ;
use Response;

class GithubController extends Controller
{
	public function getUsers(Request $request, $user){

		$client = new Client();

		try {

			$resUser = $client->get('https://api.github.com/users/'.$user);

			$resRepo = $client->get('https://api.github.com/users/'.$user.'/repos');


			return Response::json([
				"user" => json_decode($resUser->getBody()),
				"repos" => json_decode($resRepo->getBody())
			], 200);

		} catch (ClientException $e) {
			$err = (string)$e->getResponse()->getBody(true);
			return Response::json([
				"error" => $err,
			], 404);
		}

		


	}
}
