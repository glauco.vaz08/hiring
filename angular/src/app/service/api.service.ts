import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  public getUser(user: string) {
    return this.http.get('http://localhost/api/user/' + user);
  }
}
