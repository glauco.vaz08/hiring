import { HomeComponent } from './../components/home/home.component';
import { ProfileComponent } from './../components/profile/profile.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'profile/:user', component: ProfileComponent },
  { path: '**', redirectTo: 'home' }
];

export const routes = RouterModule.forRoot(appRoutes);
