import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { User } from '../../domain/user.class';
import { Repository } from '../../domain/repository.class';
import { ApiService } from '../../service/api.service';

declare let $;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [
    ApiService
  ],
  animations: [
    trigger(
      'loadingAnimation', [
        transition(':enter', [
          style({ transform: 'translateY(100%)' }),
          animate('500ms ease-out', style({ transform: 'translateY(0)' }))
        ]),
        transition(':leave', [
          style({ transform: 'translateY(0)'  }),
          animate('500ms ease-in', style({ transform: 'translateY(-100%)'  }))
        ])
      ]
    ),
    trigger(
      'reposAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)' }),
          animate('250ms linear', style({ transform: 'translateX(0)' }))
        ]),
      ]
    ),
    trigger(
      'profileAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(-100%)' }),
          animate('250ms linear', style({ transform: 'translateX(0)' }))
        ]),
      ]
    ),
  ],
})
export class ProfileComponent implements OnInit {
  
  public showLoading = true;
  public showContent = false;
  public showError = false;
  public notFound = false;
  public user: User;
  public repos: Repository[] = [];
  public repoActive: Repository;
  
  constructor(
    private service: ApiService,
    private activatedRoute: ActivatedRoute
  ) { }
  
  ngOnInit() {
    // prepare modal
    $(document).ready(function () {
      $('#modal').modal();
    });

    // get param
    this.activatedRoute.params.subscribe(
      params => {
        this.service.getUser(params['user'])
        .subscribe(
          (data: any) => {
            this.user = new User(data.user);
            for (let repo of data.repos) {
              this.repos.push(new Repository(repo));
            }
            this.showLoading = false;
          
            setTimeout(() => {
              this.showContent = true;
            }, 500);          

          },
          err => {
            this.showLoading = false;
            if(err.error){
              this.notFound = true;
            }
            setTimeout(() => {
              this.showError = true;
            }, 500);
          }
        );
      }
    );

  }
  
  openModal(repo) {
    this.repoActive = repo;
    $('#modal').modal('open');
  }

  closeModal() {
    $('#modal').modal('close');
  }
  
}
