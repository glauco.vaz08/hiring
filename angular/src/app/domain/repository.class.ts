export class Repository {

    private _name: string;
    private _url: string;
    private _description: string;
    private _language: string;
    private _createdAt: Date;

    constructor(data: any) {
        this._name = data.name;
        this._url = data.html_url;
        this._description = data.description;
        this._language = data.language;
        this._createdAt = new Date(data.created_at);
    }


    public get name(): string {
        return this._name;
    }

    public get url(): string {
        return this._url;
    }

    public get description(): string {
        return this._description;
    }

    public get language(): string {
        return this._language;
    }

    public get createdAt(): string {
        return this._createdAt.toLocaleDateString();
    }
    
}
