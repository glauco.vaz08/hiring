export class User {
    
    private _username: string;
    private _avatar: string;
    private _followers: number;
    private _following: number;
    private _name: string;
    private _company: string;
    private _blog: string;
    private _location: string;
    private _bio: string;
    private _memberAt: Date;
    
    constructor(data: any) {
        this._username = data.login;
        this._avatar = data.avatar_url;
        this._followers = data.followers;
        this._following = data.following;
        this._name = data.name;
        this._company = data.company;
        this._blog = data.blog;
        this._location = data.location;
        this._bio = data.bio;
        this._memberAt = new Date(data.created_at);
    }    


    public get username(): string {
        return this._username;
    }

    public get avatar(): string {
        return this._avatar;
    }

    public get followers(): number {
        return this._followers;
    }

    public get following(): number {
        return this._following;
    }

    public get name(): string {
        return this._name;
    }

    public get company(): string {
        return this._company;
    }

    public get blog(): string {
        return this._blog;
    }

    public get location(): string {
        return this._location;
    }

    public get bio(): string {
        return this._bio;
    }

    public get memberAt(): string {
        return this._memberAt.toLocaleDateString();
    }

}
