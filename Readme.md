## Instalação

1 - Clonar este repositório para seu computador

2 - Acessar a pasta laradock 

`$ cd hiring/laradock`

3 - Rodar os containers Docker

`# docker-compose up -d nginx hiring`

4 - Dar permissao à pasta storage

`# docker exec laradock_workspace_1 chmod 777 -R storage/`

5- Instalar as dependencias do projeto

`# docker exec laradock_workspace_1 composer install`

6- Acessar a aplicação em localhost:2000

Obs: A aplicação utiliza as portas 2000, 80 e 2222